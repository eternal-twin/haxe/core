package etwin.js;

/**
  Error class with JS semantics

  When targetting JS, this is the native `Error` class.
  On other platforms, it is a reimplementation.
**/
#if js
@:native("Error")
extern class Error {
#else
class Error {
#end
  /**
    Error message.
  **/
  public var message: String;

  /**
    Error name.
  **/
  public var name: String;

  /**
    Creates a new `Error` object.
  **/
  public function new(?message: String): Void
#if js
  ;
#else
  {
    this.name = "Error";
    this.message = message != null ? message : "";
  }
#end

  /**
    Returns a string representing the specified object.
  **/
  public function toString(): String
#if js
  ;
#else
  {
    return this.name + (this.message != "" ? (": " + this.message) : "");
  }
#end
}
