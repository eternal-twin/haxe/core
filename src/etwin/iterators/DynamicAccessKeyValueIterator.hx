package etwin.iterators;

/**
  This Key/Value iterator can be used to iterate over `etwin.DynamicAccess`.
**/
class DynamicAccessKeyValueIterator<T> {
  private var access: DynamicAccess<T>;
  private var keys: Array<String>;
  private var index: Int;

  public inline function new(access: DynamicAccess<T>) {
    this.access = access;
    this.keys = access.keys();
    index = 0;
  }

  /**
    See `Iterator.hasNext`
	**/
  public inline function hasNext(): Bool {
    return index < keys.length;
  }

  /**
    See `Iterator.next`
	**/
  public inline function next(): {
    key: String, value: T
  } {
    var key = keys[index++];
    return {value: (access[key] : T), key: key};
  }
}
