package etwin.iterators;

/**
  This iterator can be used to iterate over the values of `etwin.DynamicAccess`.
**/
class DynamicAccessIterator<T> {
  private var access: DynamicAccess<T>;
  private var keys: Array<String>;
  private var index: Int;

  public inline function new(access: DynamicAccess<T>) {
    this.access = access;
    this.keys = access.keys();
    index = 0;
  }

  /**
		See `Iterator.hasNext`
	**/
  public inline function hasNext(): Bool {
    return index < keys.length;
  }

  /**
		See `Iterator.next`
	**/
  public inline function next(): T {
    return access[keys[index++]];
  }
}
