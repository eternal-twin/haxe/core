package etwin.flash;

import etwin.flash.XMLNode;

@:native("XML")
extern class XML extends XMLNode {
  /**
    The MIME content type that is sent to the server when you call the `XML.send()` or `XML.sendAndLoad()` method.
  **/
  public var contentType: String;

  /**
    Specifies information about the XML document's `DOCTYPE` declaration.
  **/
  public var docTypeDecl: String;

  /**
    An object containing the XML file's nodes that have an `id` attribute assigned.
  **/
  public var idMap: Dynamic;

  /**
    Default setting is `false`.
  **/
  public var ignoreWhite: Bool;

  /**
    The property that indicates whether the XML document has successfully loaded.
  **/
  public var loaded: Bool;

  /**
    Automatically sets and returns a numeric value that indicates whether an XML document was successfully parsed into an XML object.
  **/
  public var status: Float;

  /**
    A string that specifies information about a document's XML declaration.
  **/
  public var xmlDecl: String;

  /**
    Creates a new XML object.

    You must use the constructor to create an XML object before you call any of
    the methods of the XML class.

    **Note**: Use the `createElement()` and `createTextNode()` methods to add
    elements and text nodes to an XML document tree.

    # Parameters
    - text: String — A string; the XML text parsed to create the new XML object.

    # See also
    - `XML.createElement()`
    - `XML.createTextNode()`

    # Example

    The following example creates a new, empty XML object:
    ```
    var my_xml: XML = new XML();
    ```

    The following example creates an XML object by parsing the XML text
    specified in the source parameter, and populates the newly created XML
    object with the resulting XML document tree:
    ```
    var other_xml: XML = new XML("<state name=\"California\"><city>San Francisco</city></state>");
    ```
  **/
  public function new(?text: String): Void;

  /**
    Creates a new XML element with the name specified in the parameter.
  **/
  public function createElement(name: String): XMLNode;

  /**
    Creates a new XML text node with the specified text.
  **/
  public function createTextNode(value: String): XMLNode;

  /**
    Parses the XML text specified in the `value` parameter, and populates the specified XML object with the resulting XML tree.
  **/
  public function parseXML(value: String): Void;

  /**
    Returns the number of bytes loaded (streamed) for the XML document.
  **/
  public function getBytesLoaded(): Int;

  /**
    Returns the size, in bytes, of the XML document.
  **/
  public function getBytesTotal(): Int;

  /**
  Loads an XML document from the specified URL and replaces the contents of the specified XML object with the downloaded XML data.
  **/
  public function load(url: String): Bool;

  /**
    Adds or changes HTTP request headers (such as `Content-Type` or `SOAPAction`) sent with `POST` actions.
  **/
  public function addRequestHeader(header: Dynamic, headerValue: String): Void;

  /**
    Encodes the specified XML object into an XML document and sends it to the specified `target` URL.
  **/
  public function send(url: String, ?target: String, ?method: String): Bool;

  /**
    Encodes the specified XML object into an XML document, sends it to the specified URL using the POST method, downloads the server's response, and loads it into the resultXMLobject specified in the parameters.
  **/
  public function sendAndLoad(url: String, resultXML: XML, ?method: String): Void;

  /**
    Invoked when Flash Player receives an HTTP status code from the server.
  **/
  public dynamic function onHTTPStatus(httpStatus: Int): Void;

  /**
    Invoked by Flash Player when an XML document is received from the server.
  **/
  public dynamic function onLoad(success: Bool): Void;

  /**
    Invoked when XML text has been completely downloaded from the server, or when an error occurs downloading XML text from a server.
  **/
  public dynamic function onData(src: String): Void;
}
