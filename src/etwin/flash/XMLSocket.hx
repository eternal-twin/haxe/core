package etwin.flash;

import etwin.flash.XML;

@:native("XMLSocket")
extern class XMLSocket {
  /**
    The number of milliseconds to wait for a connection.
  **/
  public var timeout: Float;

  /**
    Creates a new XMLSocket object.
  **/
  public function new(): Void;

  /**
    Establishes a connection to the specified Internet host using the specified TCP port.

    Returns true or false, depending on whether a connection is successfully established.
  **/
  public function connect(host: String, port: Int): Void;

  /**
    Converts the XML object or data specified in the object parameter to a string and transmits it to the server, followed by a zero (0) byte.
  **/
  public function send(data: Dynamic): Bool;

  /**
    Closes the connection specified by XMLSocket object.
  **/
  public function close(): Void;

  /**
    Invoked by Flash Player when a connection request initiated through XMLSocket.connect() has succeeded or failed.
  **/
  public dynamic function onConnect(success: Bool): Void;

  /**
    Invoked when a message has been downloaded from the server, terminated by a zero (0) byte.
  **/
  public dynamic function onData(src: String): Void;

  /**
    Invoked by Flash Player when the specified XML object containing an XML document arrives over an open XMLSocket connection.
  **/
  public dynamic function onXML(src: XML): Void;

  /**
    Invoked only when an open connection is closed by the server.
  **/
  public dynamic function onClose(): Void;
}
