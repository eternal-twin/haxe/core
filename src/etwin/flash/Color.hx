package etwin.flash;

import etwin.flash.MovieClip;

@:native("Color")
extern class Color {
	/**
	  **Deprecated**. Creates a Color object for the movie clip specified by the target_mc parameter.
  **/
	function new(target: MovieClip): Void;

	/**
	  **Deprecated**. Specifies an RGB color for a Color object.
  **/
	function setRGB(color: Int): Void;

	/**
	  **Deprecated**. Returns the R+G+B combination currently in use by the color object.
  **/
	function getRGB(): Int;

	/**
	  **Deprecated**. Sets color transform information for a Color object.
  **/
	function setTransform(transformObject: ColorTransform): Void;

	/**
	  **Deprecated**. Returns the transform value set by the last `Color.setTransform()` call.
  **/
	function getTransform(): ColorTransform;
}
