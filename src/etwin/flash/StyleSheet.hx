package etwin.flash;

import etwin.flash.TextFormat;

@:native("StyleSheet")
extern class StyleSheet {
  /**
    Creates a StyleSheet object.
  **/
  public function new(): Void;

  /**
    Returns a copy of the style object associated with the specified style (`name`).
  **/
  public function getStyle(name: String): Dynamic;

  /**
    Adds a new style with the specified name to the StyleSheet object.
  **/
  public function setStyle(name: String, style: Dynamic): Void;

  /**
    Removes all styles from the specified StyleSheet object.
  **/
  public function clear(): Void;

  /**
    Returns an array that contains the names (as strings) of all of the styles registered in this style sheet.
  **/
  public function getStyleNames(): Array<Dynamic>;

  /**
    Extends the CSS parsing capability.
  **/
  public function transform(style: Dynamic): TextFormat;

  /**
    Parses the CSS in `cssText` and loads the StyleSheet with it.
  **/
  public function parseCSS(cssText: String): Bool;

  /**
    Starts loading the CSS file into the StyleSheet.
  **/
  public function load(url: String): Bool;

  /**
    Invoked when a `load()` operation has completed.
  **/
  public dynamic function onLoad(success: Bool): Void;
}
