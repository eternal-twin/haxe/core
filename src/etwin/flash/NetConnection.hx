package etwin.flash;

@:native("NetConnection")
extern class NetConnection {
  /**
    A Boolean value that indicates whether Flash Player is connected to Flash Media Server.
  **/
  public var isConnected(default, null): Bool;

  /**
    The protocol used to establish the connection.
  **/
  public var protocol(default, null): String;

  /**
    The target Uniform Resource Identifier (URI) that was passed in by `NetConnection.connect()`.
  **/
  public var uri(default, null): String;

  /**
    Creates a NetConnection object that you can use in conjunction with a NetStream object to play back local streaming video (FLV) files.
  **/
  public function new();

  /**
    With Flash Media Server, creates a connection between Flash Player and an application on Flash Media Server and without Flash Media Server opens a local connection.
  **/
  public function connect(targetURI: Null<String>): Bool;

  /**
    Used with Flash Media Server to invoke a method defined on a client object in server-side ActionScript.
  **/
  public function call(remoteMethod: String, resultObject: Dynamic): Void;

  /**
    Used with Flash Media Server to close the connection with the server.
  **/
  public function close(): Void;

  /**
    Invoked when a status change or error is posted for the NetConnection object.
  **/
  public dynamic function onStatus(infoObject: Dynamic): Void;

  /**
    Invoked when a result object is returned by a Flash Media Server method.
  **/
  public dynamic function onResult(infoObject: Dynamic): Void;
}
