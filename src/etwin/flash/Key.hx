package etwin.flash;

import etwin.flash.KeyListener;

@:native("Key")
extern class Key {
  /**
    A list of references to all listener objects that are registered with the Key object.
  **/
  public static var _listeners(default, null): Array<KeyListener>;

  /**
    The key code value for the Backspace key (8).
  **/
  public static var BACKSPACE(default, null): Int;

  /**
    The key code value for the Tab key (9).
  **/
  public static var TAB(default, null): Int;

  /**
    The key code value for the Enter key (13).
  **/
  public static var ENTER(default, null): Int;

  /**
    The key code value for the Shift key (16).
  **/
  public static var SHIFT(default, null): Int;

  /**
    The key code value for the Control key (17).
  **/
  public static var CONTROL(default, null): Int;

  /**
    The key code value for the Control key (18).
  **/
  public static var ALT(default, null): Int;

  /**
    The key code value for the Caps Lock key (20).
  **/
  public static var CAPSLOCK(default, null): Int;

  /**
    The key code value for the Escape key (27).
  **/
  public static var ESCAPE(default, null): Int;

  /**
    The key code value for the Spacebar (32).
  **/
  public static var SPACE(default, null): Int;

  /**
    The key code value for the Page Up key (33).
  **/
  public static var PGUP(default, null): Int;

  /**
    The key code value for the Page Down key (34).
  **/
  public static var PGDN(default, null): Int;

  /**
    The key code value for the End key (35).
  **/
  public static var END(default, null): Int;

  /**
    The key code value for the Home key (36).
  **/
  public static var HOME(default, null): Int;

  /**
    The key code value for the Left Arrow key (37).
  **/
  public static var LEFT(default, null): Int;

  /**
    The key code value for the Up Arrow key (38).
  **/
  public static var UP(default, null): Int;

  /**
    The key code value for the Right Arrow key (39).
  **/
  public static var RIGHT(default, null): Int;

  /**
    The key code value for the Down Arrow key (40).
  **/
  public static var DOWN(default, null): Int;

  /**
    The key code value for the Insert key (45).
  **/
  public static var INSERT(default, null): Int;

  /**
    The key code value for the Delete key (46).
  **/
  public static var DELETEKEY(default, null): Int;

  /**
    Returns the ASCII code of the last key pressed or released.
  **/
  static function getAscii(): Int;

  /**
    Returns the key code value of the last key pressed.
  **/
  static function getCode(): Int;

  /**
    Returns a Boolean value indicating, depending on security restrictions, whether the last key pressed may be accessed by other SWF files.
  **/
  static function isAccessible(): Bool;

  /**
    Returns `true` if the key specified in `keycode` is pressed; `false` otherwise.
  **/
  static function isDown(code: Int): Bool;

  /**
    Returns `true` if the Caps Lock or Num Lock key is activated (toggled to an active state); `false` otherwise.
  **/
  static function isToggled(code: Int): Bool;

  /**
    Registers an object to receive `onKeyDown` and `onKeyUp` notification.
  **/
  static function addListener(listener: KeyListener): Void;

  /**
    Removes an object previously registered with `Key.addListener()`.
  **/
  static function removeListener(listener: KeyListener): Bool;
}
