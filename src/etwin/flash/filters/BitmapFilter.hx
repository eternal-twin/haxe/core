package etwin.flash.filters;

@:native("flash.filters.BitmapFilter")
extern class BitmapFilter {
  function new(): Void;

  function clone(): BitmapFilter;
}
