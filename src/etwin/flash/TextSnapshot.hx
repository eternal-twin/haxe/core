package etwin.flash;

@:native("TextSnapshot")
extern class TextSnapshot {
  /**
    Searches the specified TextSnapshot object and returns the position of the first occurrence of `textToFind` found at or after `startIndex`.
  **/
  function findText(startIndex: Int, textToFind: String, caseSensitive: Bool): Int;

  /**
    Returns the number of characters in a TextSnapshot object.
  **/
  function getCount(): Int;

  /**
    Returns a Boolean value that specifies whether a TextSnapshot object contains selected text in the specified range.
  **/
  function getSelected(start: Int, ?end: Int): Bool;

  /**
    Returns a string that contains all the characters specified by the corresponding `TextSnapshot.setSelected()` method.
  **/
  function getSelectedText(?includeLineEndings: Bool): String;

  /**
    Returns a string that contains all the characters specified by the `start` and `end` parameters.
  **/
  function getText(start: Int, end: Int, ?includeLineEndings: Bool): String;

  /**
    Returns an array of objects that contains information about a run of text.
  **/
  function getTextRunInfo(beginIndex: Int, endIndex: Int): Array<Dynamic>;

  /**
    Lets you determine which character within a TextSnapshot object is on or near the specified x, y coordinates of the movie clip containing the text in the TextSnapshot object.
  **/
  function hitTestTextNearPos(x: Float, y: Float, ?closeDist: Float): Float;

  /**
    Specifies the color to use when highlighting characters that were selected with the `TextSnapshot.setSelected()` method.
  **/
  function setSelectColor(color: Int): Void;

  /**
    Specifies a range of characters in a TextSnapshot object to be selected or not.
  **/
  function setSelected(start: Int, end: Int, select: Bool): Void;
}
