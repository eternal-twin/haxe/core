package etwin.flash;

import etwin.flash.MovieClip;

/**
  A MovieClip with untyped extra properties.
**/
@:native("MovieClip")
extern class DynamicMovieClip extends MovieClip implements Dynamic {
}
