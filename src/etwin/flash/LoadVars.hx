package etwin.flash;

@:native("LoadVars")
extern class LoadVars implements Dynamic<String> {
  /**
    The MIME type that is sent to the server when you call `LoadVars.send()` or `LoadVars.sendAndLoad()`.
  **/
  public var contentType: String;

  /**
    A Boolean value that indicates whether a `load` or `sendAndLoad` operation has completed, `undefined` by default.
  **/
  public var loaded: Bool;

  /**
    Creates a LoadVars object.
  **/
  public function new(): Void;

  /**
    Adds or changes HTTP request headers (such as Content-Type or SOAPAction) sent with POST actions.
  **/
  public function addRequestHeader(header: String, headerValue: String): Void;

  /**
    Downloads variables from the specified URL, parses the variable data, and places the resulting variables in `my_lv`.
  **/
  public function load(url: String): Bool;

  /**
    Sends the variables in the `my_lv` object to the specified URL.
  **/
  public function send(url: String, target: String, ?method: String): Bool;

  /**
    Posts variables in the `my_lv` object to the specified URL.
  **/
  public function sendAndLoad(url: String, targetObject: Dynamic, ?method: String): Bool;

  /**
    Returns the number of bytes downloaded by `LoadVars.load()` or `LoadVars.sendAndLoad()`.
  **/
  public function getBytesLoaded(): Int;

  /**
    Returns the total number of bytes downloaded by `LoadVars.load()` or `LoadVars.sendAndLoad()`.
  **/
  public function getBytesTotal(): Int;

  /**
    Converts the variable string to properties of the specified LoadVars object.
  **/
  public function decode(queryString: String): Void;

  /**
    Returns a string containing all enumerable variables in `my_lv`, in the MIME content encoding application/x-www-form-urlencoded.
  **/
  public function toString(): String;

  /**
    Invoked when a `LoadVars.load()` or `LoadVars.sendAndLoad()` operation has ended.
  **/
  public dynamic function onLoad(success: Bool): Void;

  /**
    Invoked when data has completely downloaded from the server or when an error occurs while data is downloading from a server.
  **/
  public dynamic function onData(src: String): Void;

  /**
    Invoked when Flash Player receives an HTTP status code from the server.
  **/
  public dynamic function onHTTPStatus(status: Int): Void;
}
