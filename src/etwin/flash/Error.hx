package etwin.flash;

/**
  Error class with AS2 semantics

  When targetting AVM1, this is the native `Error` class.
  On other platforms, it is a reimplementation.
**/
#if flash8
@:native("Error")
extern class Error {
#else
class Error {
#end

  /**
    Contains the message associated with the Error object.
  **/
  public var message: String;

  /**
    Contains the name of the Error object.
  **/
  public var name: String;

  /**
    Creates a new Error object.
  **/
  public function new(?message: String): Void
#if flash8
  ;
#else
  {
    this.name = "Error";
    this.message = message != null ? message : "";
  }
#end

  /**
    Returns a string describing the error.
  **/
  public function toString(): String
#if flash8
  ;
#else
  {
    return this.name + (this.message != "" ? (": " + this.message) : "");
  }
#end
}
