package etwin.flash;

typedef StageListener = {
/**
  Invoked when the movie enters, or leaves, full-screen mode.
**/
function onFullScreen(full: Bool): Void;

/**
  Invoked when Stage.scaleMode is set to noScale and the SWF file is resized.
**/
function onResize(): Void;
}
