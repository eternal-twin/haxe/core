package etwin.flash;

import etwin.flash.TextField;

typedef SelectionListener = {
  /**
    Notified when the input focus changes.
  **/
  // onSetFocus(?oldFocus: Dynamic, ?newFocus: Dynamic): Void;
var onSetFocus: TextField -> TextField -> Void;
}
