package etwin.flash;

@:native("SharedObject")
extern class SharedObject {
  /**
    Returns a reference to a locally persistent shared object that is available only to the current client.
  **/
  public static function getLocal(name: String, ?localPath: String, ?secure: Bool): SharedObject;

  /**
    Returns a reference to an object that can be shared across multiple clients using Flash Media Server.
  **/
  public static function getRemote(name: String, remotePath: String, ?persistence: Dynamic, ?secure: Bool): SharedObject;

  /**
    The collection of attributes assigned to the data property of the object; these attributes can be shared and/or stored.
  **/
  public var data: Dynamic;

  /**
    Purges all the data from the shared object and deletes the shared object from the disk.
  **/
  public function clear(): Void;

  /**
    Broadcasts a message to all clients connected to a shared object, including the client that sent the message.
  **/
  public function send(handler: String, ?p1: Dynamic, ?p2: Dynamic, ?p3: Dynamic, ?p4: Dynamic, ?p5: Dynamic): Void;

  /**
    Immediately writes a locally persistent shared object to a local file.
  **/
  public function flush(?minDiskSpace: Float): Dynamic;

  /**
    Connects to a remote shared object on Flash Media Server through the specified connection.
  **/
  public function connect(myConnection: NetConnection): Bool;

  /**
    Closes the connection between a remote shared object and Flash Media Server.
  **/
  public function close(): Void;

  /**
    Gets the current size of the shared object, in bytes.
  **/
  public function getSize(): Float;

  /**
    Specifies the number of times per second that a client's changes to a shared object are sent to the server.
  **/
  public function setFps(updatesPerSecond: Float): Bool;

  /**
    Invoked every time an error, warning, or informational note is posted for a shared object.
  **/
  public dynamic function onStatus(infoObject: Dynamic): Void;

  /**
     	Invoked when the client and server copies of a remote shared object are synchronized.
  **/
  public dynamic function onSync(objArray: Array<Dynamic>): Void;
}
