package etwin.flash;

@:native("XMLNode")
extern class XMLNode {
  /**
    An object containing all of the attributes of the specified XML instance.
  **/
  public var attributes: Dynamic;

  /**
    An array of the specified XML object's children.
  **/
  public var childNodes(default, null): Array<XMLNode>;

  /**
    Evaluates the specified XML object and references the first child in the parent node's child list.
  **/
  public var firstChild(default, null): XMLNode;

  /**
    An XMLNode value that references the last child in the node's child list.
  **/
  public var lastChild(default, null): XMLNode;

  /**
    The local name portion of the XML node's name.
  **/
  public var localName(default, null): String;

  /**
    If the XML node has a prefix, namespaceURI is the value of the xmlns declaration for that prefix (the URI), which is typically called the namespace URI.
  **/
  public var namespaceURI(default, null): String;

  /**
    An XMLNode value that references the next sibling in the parent node's child list.
  **/
  public var nextSibling(default, null): XMLNode;

  /**
    A string representing the node name of the XML object.
  **/
  public var nodeName: Null<String>;

  /**
    A `nodeType` value, either 1 for an XML element or 3 for a text node.
  **/
  public var nodeType(default, null): Int;

  /**
    The node value of the XML object.
  **/
  public var nodeValue: Null<String>;

  /**
    An XMLNode value that references the parent node of the specified XML object, or returns `null` if the node has no parent.
  **/
  public var parentNode(default, null): XMLNode;

  /**
    The prefix portion of the XML node name.
  **/
  public var prefix(default, null): String;

  /**
    An XMLNode value that references the previous sibling in the parent node's child list.
  **/
  public var previousSibling(default, null): XMLNode;

  /**
    The XMLNode constructor lets you instantiate an XML node based on a string specifying its contents and on a number representing its node type.
  **/
  public function new(type: Int, value: String): Void;

  /**
    Constructs and returns a new XML node of the same type, name, value, and attributes as the specified XML object.
  **/
  public function cloneNode(deep: Bool): XMLNode;

  /**
    Appends the specified node to the XML object's child list.
  **/
  public function appendChild(newChild: XMLNode): Void;

  /**
    Returns the namespace URI that is associated with the specified prefix for the node.
  **/
  public function getNamespaceForPrefix(prefix: String): String;

  /**
    Returns the prefix that is associated with the specified namespace URI for the node.
  **/
  public function  getPrefixForNamespace(nsURI: String): String;

  /**
    Inserts a `newChild` node into the XML object's child list, before the `insertPoint` node.
  **/
  public function insertBefore(newChild: XMLNode, insertPoint: XMLNode): Void;

  /**
    Removes the specified XML object from its parent.
  **/
  public function removeNode():Void;

  /**
    Specifies whether or not the XML object has child nodes.
  **/
  public function hasChildNodes(): Bool;

  /**
    Evaluates the specified XML object, constructs a textual representation of the XML structure, including the node, children, and attributes, and returns the result as a string.
  **/
  public function toString(): String;
}
