package etwin.flash;


import etwin.flash.geom.Rectangle;
@:native("Stage")
extern class Stage {
  /**
    Indicates the current alignment of the SWF file in the player or browser.
  **/
  static var align: String;

  /**
    Property (read-only); indicates the current width, in pixels, of the Stage.
  **/
  static var width(default, null): Float;

  /**
    Property (read-only); indicates the current height, in pixels, of the Stage.
  **/
  static var height(default, null): Float;

  /**
    Indicates the current scaling of the SWF file within Flash Player.
  **/
  static var scaleMode: String;

  /**
    Specifies whether to show or hide the default items in the Flash Player context menu.
  **/
  static var showMenu: Bool;

  /**
    Sets Flash Player to play the movie in full-screen mode or to take Flash Player out of full-screen mode.
  **/
  static var displayState: String;

  /**
    Returns the width of the monitor that will be used when going to full screen size, if that state is entered immediately.
  **/
  static var fullScreenWidth: Int;

  /**
    Returns the height of the monitor that will be used when going to full screen size, if that state is entered immediately.
  **/
  static var fullScreenHeight: Int;

  /**
    Sets Flash Player to scale a specific region of the stage to full-screen mode.
  **/
  static var fullScreenSourceRect: Rectangle<Int>;

  /**
    Indicates whether GPU compositing is available and in use.
  **/
  static var wmodeGPU: Bool;

  /**
    Detects when a SWF file is resized (but only if Stage.scaleMode = "noScale").
  **/
  static function addListener(listener: StageListener): Void;

  /**
    Removes a listener object created with `addListener()`.
  **/
  static function removeListener(listener: StageListener): Void;
}
