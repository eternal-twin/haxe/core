package etwin.flash;

import etwin.flash.SelectionListener;

@:native("Selection")
extern class Selection {
  /**
    Returns the index at the beginning of the selection span.
  **/
  public static function getBeginIndex(): Int;

  /**
    Returns the ending index of the currently focused selection span.
  **/
  public static function getEndIndex(): Int;

  /**
    Returns the index of the blinking insertion point (caret) position.
  **/
  public static function getCaretIndex(): Int;

  /**
    Returns a string specifying the target path of the object that has focus.
  **/
  public static function getFocus(): String;

  /**
    Gives focus to the selectable (editable) text field, button, or movie clip, specified by the `newFocus` parameter.
  **/
  public static function setFocus(newFocus: Dynamic): Bool;

  /**
    Sets the selection span of the currently focused text field.
  **/
  public static function setSelection(beginIndex: Int, endIndex: Int): Void;

  /**
    Registers an object to receive keyboard focus change notifications.
  **/
  static function addListener(listener: SelectionListener): Void;

  /**
    Removes an object previously registered with `Selection.addListener()`.
  **/
  static function removeListener(listener: SelectionListener): Bool;
}
