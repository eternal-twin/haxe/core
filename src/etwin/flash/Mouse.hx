package etwin.flash;

@:native("Mouse")
extern class Mouse {
  /**
    Displays the mouse pointer in a SWF file.
  **/
  static function show(): Int;

  /**
    Hides the pointer in a SWF file.
  **/
  static function hide(): Int;

  /**
    Registers an object to receive notifications of the `onMouseDown`, `onMouseMove`, `onMouseUp`, and `onMouseWheel` listeners.
  **/
  static function addListener(listener: MouseListener): Void;

  /**
    Removes an object that was previously registered with `addListener()`.
  **/
  static function removeListener(listener: MouseListener): Bool;
}
