package etwin.flash;

import etwin.flash.MovieClip;
import etwin.flash.MovieClipLoaderListener;
import etwin.flash.MovieClipLoaderProgress;

@:native("MovieClipLoader")
extern class MovieClipLoader {
  /**
    Specifies whether Flash Player should attempt to download a policy file from the loaded object's server before beginning to load the object itself.
  **/
  public var checkPolicyFile: Bool;

  /**
    Creates a MovieClipLoader object that you can use to implement a number of listeners to respond to events while a SWF, JPEG, GIF, or PNG file is downloading.
  **/
  public function new(): Void;

  /**
    Returns the number of bytes loaded and the total number of bytes of a file that is being loaded by using `MovieClipLoader.loadClip();` for compressed movies, returns the number of compressed bytes.
  **/
  public function getProgress(target: MovieClip): MovieClipLoaderProgress;

  /**
    Loads a SWF, JPEG, progressive JPEG, unanimated GIF, or PNG file into a movie clip in Flash Player while the original movie is playing.
  **/
  public function loadClip(url: String, target: MovieClip): Bool;

  /**
    Removes a movie clip that was loaded by using `MovieClipLoader.loadClip()`.
  **/
  public function unloadClip(target: MovieClip): Bool;

  /**
    Registers an object to receive notification when a `MovieClipLoader` event handler is invoked.
  **/
  public function addListener(listener: MovieClipLoaderListener): Bool;

  /**
    Removes the listener that was used to receive notification when a `MovieClipLoader` event handler was invoked.
  **/
  public function removeListener(listener: MovieClipLoaderListener): Bool;

  /**
    Invoked when the actions on the first frame of the loaded clip have been executed.
  **/
  public dynamic function onLoadInit(target: MovieClip): Void;

  /**
    Invoked when a call to `MovieClipLoader.loadClip()` has begun to download a file.
  **/
  public dynamic function onLoadStart(target: MovieClip): Void;

  /**
    Invoked every time the loading content is written to the hard disk during the loading process (that is, between `MovieClipLoader.onLoadStart` and `MovieClipLoader.onLoadComplete`).
  **/
  public dynamic function onLoadProgress(target: MovieClip, loaded: Int, total: Int): Void;

  /**
    Invoked when a file that was loaded with `MovieClipLoader.loadClip()` is completely downloaded.
  **/
  public dynamic function onLoadComplete(target: MovieClip, httpStatus: Int): Void;

  /**
    Invoked when a file loaded with `MovieClipLoader.loadClip()` has failed to load.
  **/
  public dynamic function onLoadError(target: MovieClip, error: String, httpStatus: Int): Void;
}
