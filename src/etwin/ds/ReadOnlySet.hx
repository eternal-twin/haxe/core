package etwin.ds;

/**
  A read-only set, which cannot be directly mutated.

  Unlike `FrozenSet`, contents may still be indirectly
  mutated through other references to the underlying set.

  All non-mutating methods of `Set` are available.
**/
@:forward(exists, iterator, toString)
abstract ReadOnlySet<T>(Set<T>) from Set<T> {
  @:to @:noCompletion @:dox(hide)
  public inline function toIterable(): Iterable<T> {
    // Because the values are always identical to the keys,
    // it's fine to return the underlying map as an iterable.
    return cast this;
  }
}

