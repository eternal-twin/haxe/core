package etwin.ds;

import haxe.macro.Context;
import haxe.macro.Expr;

/**
  An immutable map, whose contents can never change.

  All non-mutating methods of `Map` are available.
**/
@:forward(exists, keys, iterator, toString)
abstract FrozenMap<K, V>(Map<K, V>) to ReadOnlyMap<K, V> {
  // Using String instead of Dynamic because Map<Dynamic, V> doesn't exist
  private static var EMPTY(default, never): FrozenMap<String, Dynamic> = new FrozenMap(new Map());

  private inline function new(map: Map<K, V>): Void {
    this = map;
  }

  @:arrayAccess @:dox(hide)
  public inline function get(k: K): V {
    return this.get(k);
  }

  /**
    Creates a new `FrozenMap` containing the elements of the given iterable or iterator.
  **/
  macro public static function from<K, V>(items: ExprOf<KeyValueIterable<K, V>>): ExprOf<FrozenMap<K, V>> {
    return macro {
      var __iter = $items.keyValueIterator();
      new etwin.ds.FrozenMap([for (__kv in __iter) __kv.key => __kv.value]);
    };
  }

  /**
    Creates a new `FrozenMap` containing the elements of the given `Map`.
  **/
  macro public static function fromMap<K, V>(map: ExprOf<Map<K, V>>): ExprOf<FrozenMap<K, V>> {
    return macro {
      var __map = $map;
      new etwin.ds.FrozenMap([for (__key in __map.keys()) __key => etwin.ds.FrozenMap.removeNull(__map[__key])]);
    };
  }

  /**
    Unsafely converts the given map into a `FrozenMap`.

    No copy will be done; you must ensure that `map` won't be further modified.
  **/
  public static inline function uncheckedFrom<K, V>(map: Map<K, V>): FrozenMap<K, V> {
    return new FrozenMap(map);
  }

  /**
    Creates a new `FrozenMap` containing the given elements.

    ```hx
    var map: FrozenMap<String, Int> = FrozenMap.of("foo" => 1, "bar" => 2);
    ```
  **/
  macro public static function of<K, V>(items: Array<Expr>): ExprOf<FrozenMap<K, V>> {
    if (items.length == 0) {
      return macro etwin.ds.FrozenMap.empty();
    }

    for (item in items) {
      if (!item.expr.match(EBinop(Binop.OpArrow, _, _))) {
        Context.error("Expected expression of the form <expr> => <expr>", item.pos);
      }
    }
    return macro new etwin.ds.FrozenMap([$a{items}]);
  }

  /**
    Creates an empty `FrozenMap`.
  **/
  public static inline function empty<K, V>(): FrozenMap<K, V> {
    return cast FrozenMap.EMPTY;
  }
}
