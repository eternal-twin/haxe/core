package etwin.ds;

import etwin.Symbol;

/**
  The WeakMap abstract type allows to map object keys to arbitrary values.

  A WeakMap can be used to safely attach a value to an object. It is the
  preferred way to store custom data attached to objects from the `hf`
  package.

  This should be preferred over the standard `haxe.ds.WeakMap` class
  because the standard class does not support the `flash` target.
**/
abstract WeakMap<K: {}, V>(Symbol) {

  public inline function new(?name: String): Void {
    this = new Symbol(name);
  }

  @:arrayAccess
  public inline function get(k: K): Null<V> {
    return Reflect.field(k, this.toString());
  }

  public inline function set(k: K, v: V): Void {
    Reflect.setField(k, this.toString(), v);
    #if flash8
    // Make the property non-enumerable
    untyped ASSetPropFlags(k, this.toString(), 1);
    #end
  }

  public inline function getOrInsert(k: K, inserter: K -> V): V {
    if (exists(k)) {
      return get(k);
    } else {
      var v = inserter(k);
      set(k, v);
      return v;
    }
  }

  @:arrayAccess
  @:noCompletion
  public inline function arrayWrite(k: K, v: V): V {
    set(k, v);
    return v;
  }

  public inline function exists(k: K): Bool {
    return Reflect.hasField(k, this.toString());
  }

  public inline function remove(k: K): Void {
    Reflect.deleteField(k, this.toString());
  }
}
