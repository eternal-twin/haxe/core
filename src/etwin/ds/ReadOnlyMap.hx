package etwin.ds;

/**
  A read-only map, which cannot be directly mutated.

  Unlike `FrozenMap`, contents may still be indirectly
  mutated through other references to the underlying array.

  All non-mutating methods of `Map` are available.
**/
@:forward(exists, keys, iterator, toString)
abstract ReadOnlyMap<K, V>(Map<K, V>) from Map<K, V> {

  @:arrayAccess @:dox(hide)
  public inline function get(k: K): V {
    return this.get(k);
  }

  @:to @:noCompletion @:dox(hide)
  public inline function toIterable(): Iterable<V> {
    return cast this;
  }
}

