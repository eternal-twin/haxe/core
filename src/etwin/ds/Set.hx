package etwin.ds;

import haxe.ds.EnumValueMap;
import haxe.ds.IntMap;
import haxe.ds.ObjectMap;
import haxe.ds.StringMap;
import haxe.macro.Expr;
import Map.IMap;

/**
  An unordered collection of unique elements.
**/
@:multiType(T)
abstract Set<T>(IMap<T, T>) {
  /**
    Creates a new set.
  **/
  public function new();

  /**
    Returns `true` if `item` has a mapping, `false` otherwise.

    If `key` is null, the result is unspecified.
  **/
  public inline function exists(item: T): Bool {
    return this.exists(item);
  }

  /**
    Adds `item` to `this` set.

    Does nothing if `item` is already present in the set.
  **/
  public inline function add(item: T): Void {
    this.set(item, item);
  }

  /**
    Adds all the items of the given iterable to `this` set.
  **/
  public inline function addAll(items: Iterable<T>): Void {
    for (item in items) {
      add(item);
    }
  }

  /**
    Removes `item` from `this` set, returning true if the removal was successful.
  **/
  public inline function remove(item: T): Bool {
    return this.remove(item);
  }

  /**
    Returns an `Iterator` over the values of this set.

    The order of values is undefined.
  **/
  public inline function iterator(): Iterator<T> {
    return this.keys();
  }

  /**
    Returns a String representation of `this` Set.
  **/
  public function toString(): String {
    var items = this.keys();
    if (!items.hasNext()) {
      return "{}";
    }

    var s = new StringBuf();
    s.add("{");
    s.add(Std.string(items.next()));

    for (item in items) {
      s.add(", ");
      s.add(Std.string(items.next()));
    }

    s.add("}");
    return s.toString();
  }

  /**
    Creates a new `Set` containing the elements of the given iterable or iterator.

    ```hx
    var elems = ["foo", "bar"];
    var set: Set<String> = Set.from(elems);
    ```
  **/
  macro public static function from<T>(items: ExprOf<Iterable<T>>): ExprOf<Set<T>> {
    return macro etwin.ds.Set.__fromMapUnchecked([for (item in $items) item => item]);
  }

  /**
    Creates a new `Set` containing the given elements.

    ```hx
    var set: Set<String> = Set.of("foo", "bar");
    ```
  **/
  macro public static function of<T>(items: Array<ExprOf<T>>): ExprOf<Set<T>> {
    if (items.length == 0) {
      return macro new etwin.ds.Set();
    }

    var stmts = [];
    var exprs = [];
    for (item in items) {
      var isLiteral = item.expr.match(EConst(_)) && !item.expr.match(EConst(CRegexp(_)));
      if (isLiteral) {
        exprs.push(macro $item => $item);
      } else {
        var name = "__item" + exprs.length;
        stmts.push(macro var $name = $item);
        exprs.push(macro $i{name} => $i{name});
      }
    }
    stmts.push(macro etwin.ds.Set.__fromMapUnchecked([$a{exprs}]));
    return macro $b{stmts};
  }

  // Implementation details for `of` and `from` macros; only correct when `map`
  // contains only identity mappings.
  private static inline function __fromMapUnchecked<T>(map: Map<T, T>): Set<T> {
    return cast map;
  }

  // Implementation details for multitype specialization

  @:to
  private static inline function __toStringMap(t: IMap<String, String>): IMap<String, String> {
    return new StringMap();
  }

  @:to
  private static inline function __toIntMap(t: IMap<Int, Int>): IMap<Int, Int> {
    return new IntMap();
  }

  @:to
  private static inline function __toEnumValueMap<T: EnumValue>(t: IMap<T, T>): IMap<T, T> {
    return new EnumValueMap();
  }

  @:to
  private static inline function __toObjectMap<T: {}>(t: IMap<T, T>): IMap<T, T> {
    return new ObjectMap();
  }
}
