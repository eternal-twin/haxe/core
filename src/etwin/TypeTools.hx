package etwin;

import etwin.ds.Nil;

/**
  Extension methods for safe type casting.

  Four families of methods are provided:

  - `is*`: type tests;
  - `as*`: type casts returning `Nil.none()` on failure;
  - `asNull*`: type casts returning `null` on failure;
  - `cast*`: type casts throwing an exception on failure, with an optional message.
**/
class TypeTools {

  public static inline function is<T>(v: Dynamic, cls: Class<T>): Bool {
    // We cannot use a raw `__instanceof__` because of enum
    // instances (which are arrays) and interfaces (on JS only).
    return Std.is(v, cls);
  }

  public static inline function isInt(v: Dynamic): Bool {
    #if flash8
    return untyped (__physeq__(__typeof__(v), "number") && __physeq__(v | 0, v));
    #elseif js
    return untyped __js__("typeof(v) === \"number\" && (v | 0) === v");
    #else
    return Std.is(v, Int);
    #end
  }

  public static inline function isFloat(v: Dynamic): Bool {
    #if flash8
    return untyped __physeq__(__typeof__(v), "number");
    #elseif js
    return untyped __js__("typeof(v) === \"number\"");
    #else
    return Std.is(v, Float);
    #end
  }

  public static inline function isString(v: Dynamic): Bool {
    #if flash8
    return untyped __physeq__(__typeof__(v), "string");
    #elseif js
    return untyped __js__("typeof(v) === \"string\"");
    #else
    return Std.is(v, String);
    #end
  }

  public static inline function isBool(v: Dynamic): Bool {
    #if flash8
    return untyped __physeq__(__typeof__(v), "boolean");
    #elseif js
    return untyped __js__("typeof(v) === \"boolean\"");
    #else
    return Std.is(v, Bool);
    #end
  }

  public static inline function as<T>(v: Dynamic, cls: Class<T>): Nil<T> {
    return is(v, cls) ? v : Nil.none();
  }

  public static inline function asInt(v: Dynamic): Nil<Int> {
    return isInt(v) ? v : Nil.none();
  }

  public static inline function asFloat(v: Dynamic): Nil<Float> {
    return isFloat(v) ? v : Nil.none();
  }

  public static inline function asString(v: Dynamic): Nil<String> {
    return isString(v) ? v : Nil.none();
  }

  public static inline function asBool(v: Dynamic): Nil<Bool> {
    return isBool(v) ? v : null;
  }

  public static inline function asNullable<T>(v: Dynamic, cls: Class<T>): Null<T> {
    return is(v, cls) ? v : null;
  }

  public static inline function asNullInt(v: Dynamic): Null<Int> {
    return isInt(v) ? v : null;
  }

  public static inline function asNullFloat(v: Dynamic): Null<Float> {
    return isFloat(v) ? v : null;
  }

  public static inline function asNullString(v: Dynamic): Null<String> {
    return isString(v) ? v : null;
  }

  public static inline function asNullBool(v: Dynamic): Null<Bool> {
    return isBool(v) ? v : null;
  }

  public static inline function castAs<T>(v: Dynamic, cls: Class<T>, ?msg: String): T {
    return is(v, cls) ? v : typeError(v, cls, msg);
  }

  public static inline function castInt(v: Dynamic, ?msg: String): Int {
    return isInt(v) ? v : typeError(v, "Int", msg);
  }

  public static inline function castFloat(v: Dynamic, ?msg: String): Int {
    return isFloat(v) ? v : typeError(v, "Float", msg);
  }

  public static inline function castString(v: Dynamic, ?msg: String): String {
    return isString(v) ? v : typeError(v, "String", msg);
  }

  public static inline function castBool(v: Dynamic, ?msg: String): Bool {
    return isString(v) ? v : typeError(v, "Bool", msg);
  }

  private static function typeError(v: Dynamic, cls: Dynamic /* String or Class<Dynamic> */, msg: Null<String>): Dynamic {
    var expected: String = isString(cls) ? cls : Type.getClassName(cls);
    var actual: String = switch Type.typeof(v) {
      case TNull: "null";
      case TInt: "Int";
      case TFloat: "Float";
      case TBool: "Bool";
      case TObject: "object";
      case TFunction: "function";
      case TClass(c): Type.getClassName(c);
      case TEnum(e): Type.getEnumName(e);
      case TUnknown: "unknown";
    };

    if (msg == null) {
      msg = 'TypeError: expected $expected, got $actual';
    } else {
      msg = 'TypeError: $msg (expected $expected, got $actual)';
    }

    // TODO: add cross-platform etwin.TypeError?
    throw new etwin.Error(msg);
  }
}
