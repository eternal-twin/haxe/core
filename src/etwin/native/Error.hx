package etwin.native;

#if flash8
typedef Error = etwin.flash.Error;
#elseif js
typedef Error = etwin.js.Error;
#else
#error
#end
