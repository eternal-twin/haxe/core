# 0.2.0 (2023-03-15)

- **[Feature]** Add the `etwin.TypeTools` class, providing extension methods for type casting.
- **[Feature]** Bring `etwin.ds.Symbol`'s API to parity with Haxe 4's `js.lib.Symbol`.
- **[Feature]** API additions to `etwin.ds.Weak{Map, Set}`:
    - Add an optional `name` argument to constructors.
    - Add `WeakMap.getOrInsert(k: K, inserter: K -> V) -> V`.
- **[Feature]** Add `Nil.mapOr`.
- **[Feature]** Add static methods `etwin.ds.FrozenArray.{concat, map, filter, slice, reverse, sort}`
    that return fresh `FrozenArray`s.
- **[Breaking change]** `etwin.ds.{ReadOnly, Weak}{Map, Set}` are now abstract types.
    - `WeakMap` and `ReadOnlyMap` now support array indexing syntax. 
- **[Breaking change]** `etwin.ds.Weak{Map, Set}` now require the key type to be an object.
- **[Fix]** Fix `etwin.ds.{FrozenMap, FrozenSet, Set}.of` when called with no arguments.
- **[Fix]** Improve documentation on `etwin.ds.*` types.

# 0.1.6 (2021-08-31)

- **[Fix]** Add `etwin.flash.Key.ALT`.
- **[Fix]** Add `implements Dynamic<String>` to `etwin.flash.LoadVars`.

# 0.1.5 (2021-04-22)

- **[Fix]** Fix `Nil` being broken when full DCE is enabled on `flash8` target.

# 0.1.4 (2021-04-21)

- **[Fix]** Fix reference to native `flash.geom.ColorTransform` in `BitmapData`.

# 0.1.3 (2021-04-20)

- **[Fix]** Fix various compilation errors.

# 0.1.2 (2021-04-19)

- **[Fix]** Fix `Nil.or` macro not typechecking correctly.

# 0.1.1 (2021-04-19)

- **[Fix]** Remove references to the `ef` package  in macros.

# 0.1.0 (2020-04-18)

- **[Feature]** First release.
